$(document).ready(function () {

    $('#sales_welcome_switch').click(function () {

        $('.welcome_block').hide(2000);

    });

    function disableBack() {
        window.history.forward()
    }

    // window.onload = disableBack();

    window.onpageshow = function (evt) {
        if (evt.persisted) disableBack()
    }

    $(".js-skill-input").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: $(".js-skill-input").data('route'),
                data: {
                    limit: 2,
                    searchData: request.term
                },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            plink: item.skill,
                            label: item.skill
                        }
                    }));
                }
            });
        },
    });

});