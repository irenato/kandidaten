<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kandidate extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $table = "kandidates";
}
