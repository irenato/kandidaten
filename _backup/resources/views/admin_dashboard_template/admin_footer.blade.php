<div id="attachscv" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 40%;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">CSV upload form</h4>
            </div>
            <div class="modal-body">
                <!-- Form -->
                <form method="post" action="{{url('admin/data/csv')}}" name="upload_file" id="upload_file"
                      enctype="multipart/form-data">
                    {{ csrf_field() }}
                    Select file : <input type='file' name='attach_csv' id='attach_csv' class='form-control' required=""><br>
                    <span id="errormessage"></span>
                    <p style="color: red">* Download Sample File from <a href="{{url('admin/upload/exportcsv')}}"
                                                                         id="Exportcsv">here</a></p>
                    <p style="color: red">* Please Select Only CSV format</p>
                    <button class="btn btn-primary" id="upload" name="upload" type="submit">Upload</button>
                </form>
            </div>

        </div>

    </div>
</div>
@include('includes.adminChangePasswordPopup')
<div id="changeProfile" class="modal fade" role="dialog" style="display: none;">
    <div class="modal-dialog" style="width: 40%;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload Profile Pic</h4>
            </div>
            <div class="modal-body">
                <!-- Form -->
                <form method="post" name="upload_pic" action="{{url('dashboard/profile/update')}}" id="upload_pic"
                      enctype="multipart/form-data">
                    {{ csrf_field() }}
                    Select file : <input type='file' name='attach_pic' id='attach_pic' class='form-control' required=""><br>
                    <span id="errormessage"></span>
                    <p style="color: red">* Please Select Only Png and jpg format</p>
                    <button class="btn btn-primary" id="upload_profile" name="upload_profile" type="submit">Upload
                    </button>
                </form>
            </div>

        </div>

    </div>
</div>
<input type="hidden" id="url" value='{{ Request::url() }}'>
<footer class="m-grid__item		m-footer ">
    <div class="m-container m-container--fluid m-container--full-height m-page__container">
        <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
            <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
							<span class="m-footer__copyright">
                2018 &copy; @Registered1
                                <!-- <a href="#" class="m-link">
                                  Keenthemes
                                </a> -->
              </span>
            </div>
            <div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
                <a href="http://www.argon-strategy.com/impressum.html" class="pull-right" style="margin: 10px 40px 0 0;">Impressum</a>
                <ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
                    <!-- <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">
                                About
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="#"  class="m-nav__link">
                            <span class="m-nav__link-text">
                                Privacy
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">
                                T&C
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Purchase
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__item m-nav__item">
                        <a href="#" class="m-nav__link" data-toggle="m-tooltip" title="Support Center" data-placement="left">
                            <i class="m-nav__link-icon flaticon-info m--icon-font-size-lg3"></i>
                        </a>
                    </li> -->
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- end::Footer -->
</div>
<!-- end:: Page -->
<!--begin::Base Scripts -->
<script src="{{url('assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
<script src="{{url('assets/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script><!-- 
<script src="{{url('assets/demo/default/custom/components/datatables/base/dashboard_task.js')}}" type="text/javascript"></script> -->
<!--end::Base Scripts -->
<!--begin::Page Vendors -->
<script src="{{url('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
<!--end::Page Vendors -->
<!--begin::Page Snippets -->
<!-- <script src="assets/app/js/dashboard.js" type="text/javascript"></script> -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script src="{{url('assets/app/js/dashboard.js')}}" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="{{url('assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js')}}"
        type="text/javascript"></script>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="{{url('assets/app/js/script.js')}}" type="text/javascript"></script>
<!-- <script src="{{url('assets/demo/default/custom/components/datatables/base/data_comments.js')}}" type="text/javascript"></script> -->
<!--End Vase Script -->
<!--end::Page Snippets -->
<style type="text/css">
    .m-aside-left--minimize .m-aside-menu .m-menu__nav {
        padding: 30px 0 30px 0;
        width: 6%;
    }

    #TaskModal {
        display: none;
    }

    .comment_div li {
        width: 100% !important;
    }

    .m-quick-sidebar__content {
        padding: 61px 14px;
    }
</style>
<script>

    $('#m_quicksearch_input1').keyup(function () {
        var u = $('#url').val();
        var qs = this;
        var query = $('#m_quicksearch_input1').val();
        //var url = Route::current()->getName();

        if (query.length === 0) {
            console.log(query + "is empty");
            $('.m-dropdown__body').css('display', 'none');
        }
        else {
            url = '/dashboard/search';
            $.ajax({
                url: url,
                data: {
                    query: query,
                },
                dataType: 'html',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    $('.m-dropdown__content1').html(response);
                    $('.m-dropdown__body').css('display', 'block');
                }
            });
        }

    });
</script>
@yield('inline-js')
</body>
</html>
