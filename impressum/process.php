﻿<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>Kontaktaufnahme - IT Freiberufler, Personalvermittlung</title>
<meta name="Description" content="Kontaktaufnahme - Flexibilität steht bei der Argon im Vordergrund. Egal ob Sie einen Spezialisten für ein paar Tage oder ein Jahr benötigen. Argon wird den passenden Kandidaten für Sie finden. ">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <!--Style-->
    <link href="https://fonts.googleapis.com/css?family=Abel" rel="stylesheet">
    <link href="plugins/font-awesome-4.6.3/css/font-awesome.css" rel="stylesheet" />
    <link href="plugins/Swiper-3.3.1/dist/css/swiper.min.css" rel="stylesheet" />
    <link href="plugins/ionicons-2.0.1/css/ionicons.min.css" rel="stylesheet" />
    <link href="plugins/bootstrap-3.3.6-dist/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="styles/style.css" rel="stylesheet" /><base href="/impressum">
</head>
<body >
    <!--Navigation-->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="row" id="headerTop">
                <div class="col-xs-9">
                    <p>
                        Melchiorstraße 22 <br />
                        72654 Neckartenzlingen
                    </p>
                    <div class="pull-right">
                        <div class="copyright">
                            <ul>
                                <li>
                                    <a href="https://www.facebook.com/pages/Argon-Strategy-GmbH/430299343807616" target="_blank">
                                        <span class="fa fa-facebook"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.linkedin.com/company/argon-strategy-gmbh" target="_blank">
                                        <span class="fa fa-linkedin"></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xs-3">
                    <span class="fa fa-phone"></span>
                    +49 176 / 63444 962
                    <div>
                        <a href="mailto:honegg@argonstrategy.com">
                            honegg@argonstrategy.com
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12" id="menuHeader">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu" aria-expanded="false">
                            <span></span>
                        </button>
                        <a class="navbar-brand" href="index.html">
                            <h1>
                                Argon Strategy GmbH

                            </h1>
                            <h2>
                                IT-Personaldienstleister
                            </h2>
                        </a>
                    </div>

                    <div class="collapse navbar-collapse" id="menu">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="add-scroll">
                                <a href="/impressum/index.html#banner">
                                    Start
                                </a>
                            </li>
                            <li class="add-scroll">
                                <a href="/impressum/index.html#info">
                                    Info
                                </a>
                            </li>
                            <li class="add-scroll">
                                <a href="/impressum/index.html#leistungen">
                                    Leistungen
                                </a>
                            </li>
                            <li class="add-scroll">
                                <a href="/impressum/index.html#referenzen">
                                    Referenzen
                                </a>
                            </li>
                            <li class="add-scroll">
                                <a href="/impressum/contact.html">
                                    Kontakt
                                </a>
                            </li>
                            <li>
                                <a href="/impressum/impressum.html">
                                    Impressum
                                </a>
                            </li>

                            <li>
                                <a href="/impressum/blog.html">
                                    blog
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>



<?php
$to = 'honegg@argonstrategy.com'; //$_POST['email'];
$subject = $_POST['name'];
$txt =$_POST['name'];
$txt ="Name =  '".$_POST['name']."' \n Email =  '".$_POST['email']."' \n Telefon =  '".$_POST['phone']."' \n Nachricht =  '".$_POST['text']."'";


$headers = "From: " . $_POST['email']; // . "\r\n" .
//"CC: honegg@argonstrategy.com";
//$headers = "From: nagy.david@innostart.hu" . "\r\n" .
//"CC: nagy.david@innostart.hu";
mail($to,$subject,$txt,$headers);
?>
<div class="container" style="margin-top:27%">
            <div class="row">
                
                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-left: 7%;">
                     
                  <div class="col-md-1" style="margin-top: 20px;font-size: 25px; margin-left:-13%;"><span class="glyphicon glyphicon-ok-sign green"></span> </div>

<div class="col-md-11"><h3 align="center" style="margin-left:-12%;">Vielen Dank für Ihre Kontaktaufnahme! Wir melden uns innerhalb von 24 Stunden bei Ihnen.</h3></div>
                  
                </div>
            </div>
   </div>
   


<!-- Google Code for kontaktformular bgeschickt Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 864769021;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "GhP4CKz1jm0Q_aetnAM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/864769021/?label=GhP4CKz1jm0Q_aetnAM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>



</body>
</html>