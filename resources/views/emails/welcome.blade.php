@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            {{ str_replace(' ', '&nbsp;', config('app.name')) }}
        @endcomponent
    @endslot

    {{-- Body --}}
    # Willkommen!

    Hallo {{$user->client_name}}!
    Vielen Dank, dass du unserer Community auf kandidaten.org beigetreten bist!

    Anbei sind noch einige Unterlagen bezüglich unserer DSGVO Bestimmung. Falls du irgendwelche Fragen hast, kannst du dich jederzeit an uns unter der +49 7127 2097 410 wenden.

    Viele Grüße und bis bald,
    Argon Strategy GmbH

    Datenschutzbestimmung:
    Einverständniserklärung in die Erhebung und Verarbeitung von Daten durch die Argon Strategy GmbH. Für unseren Dienst erfolgt die Erhebung und Verarbeitung folgender personenbezogener Daten:
    • Name
    • Telefonnummer
    • E-Mail-Adresse
    • Lebenslauf
    • Gehaltsvorstellung
    • Technische Expertise
    • Sprachniveau
    • Bild
    Diese Daten werden auf dem Server von Argon Strategy GmbH gespeichert und können nur von berechtigten Personen eingesehen werden. Wir versichern hiermit, dass die von uns durchgeführte EDV auf der Grundlage geltender Gesetze erfolgt und für das Zustandekommen des Vertragsverhältnisses notwendig ist. Darüber hinaus benötigt es für jede weitere Datenerhebung die Zustimmung des Nutzers.
    Nutzerrechte
    Der Unterzeichnende hat das Recht, diese Einwilligung jederzeit ohne Angabe einer Begründung zu widerrufen. Weiterhin können erhobene Daten bei Bedarf korrigiert, gelöscht oder deren Erhebung eingeschränkt werden. Auf Anfrage können Sie unter der untenstehenden Adresse eine detaillierte Auskunft über den Umfang der von uns vorgenommenen Datenerhebung verlangen. Auch kann eine Datenübertragung angefordert werden, sollte der Unterzeichnende eine Übertragung seiner Daten an eine dritte Stelle wünschen.
    Folgen des Nicht-Unterzeichnens
    Der Unterzeichnende hat das Recht, dieser Einwilligungserklärung nicht zuzustimmen – da unser Dienst jedoch auf die Erhebung und Verarbeitung genannter Daten angewiesen sind, würde eine Nichtunterzeichnung eine Inanspruchnahme des Dienstes ausschließen.
    Kontakt
    Beschwerden, Auskunftsanfragen und andere Anliegen sind an folgende Stelle zu richten:
        Mario Honegg (Argon Strategy GmbH)
        Melchiorstr. 22
        72654 Neckartenzlingen
    Zustimmung durch den Nutzer
    Hiermit versichert der Nutzer, der Erhebung und der Verarbeitung seiner Daten durch die Argon Strategy GmbH zuzustimmen und über seine Rechte belehrt worden zu sein.

    Thanks,
    {{ config('app.name') }}

    {{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} {{ config('app.name') }}. All rights reserved.
        @endcomponent
    @endslot
@endcomponent