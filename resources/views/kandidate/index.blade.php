@extends('layouts.candidates')
@section('template')
    <style>
        .candidate-profile {
            margin-bottom: 30px;
        }
        .candidate-profile .profile-details {
            padding: 10px;
            border-top: 0;
            height: 350px;
        }
        .candidate-number {
            float: right;
            margin: 0 0 -30px;
            position: relative;
            z-index: 2;
            top: 5px;
            right: 5px;
            padding: 3px 11px;
            background: #fff;
            border-radius: 19px;
        }
        .m-content.admin-content strong{
            font-family: "Avenir-Heavy";
        }
        .m-content.admin-content{
            font-family: "Avenir-Roman";
        }
        .profile-details{
            background: #fff;
        }
        .picture{
            border-radius: 10px 10px 0 0;
            background: #fff;
            position: relative;
        }
        .picture img{
            border-radius: 10px;
            background: #fff;
            height: 223px;
            object-fit: cover;
        }
        .m-page-title{
            display: none;
        }
        .profile-details{
            position: relative;
            border-radius: 0 0 7px 7px;
        }
        .profile-details strong{
            display: block;
        }
        .candidate-profile:hover > div{
            box-shadow: 0 8px 10px rgba(0,0,0,.2);
        }
        .candidate-profile:hover .profile-button a{
            background: #007BE5;
            color: #fff;
        }
        .img-info td{
            width: 50%;
        }
        .img-info{
            width: 100%;
            position: absolute;
            bottom: -12px;
            left: 0;
            font-size: 14px;
            z-index: 1;
        }
        .img-info div{
            background: #fff;
            border-radius: 10px;
            padding: 0px 15px;
            width: fit-content;
            margin: 0 auto;
            text-align: center;
        }
        .firstName{
            font-size: 18px;
            margin-top: 10px;
            min-height: 54px;
        }
        .Einsatzort{
            font-size: 14px;
        }
        .Einsatzort strong{
            font-size: 16px;
        }
        .Fähigkeiten{
            font-size: 12px;
        }
        .Fähigkeiten strong{
            font-size: 16px;
        }
        .profile-button{
            text-align: center;
            position: absolute;
            bottom: 10px;
            width: 94%;
            left: 3%;
            right: 3%;
        }
        .profile-button a{
            display: block;
            color: #434656;
            font-size: 14px;
            font-weight: 600;
            padding: 8px 0;
        }
        .candidates-filter label {
            font-size: 17px !important;
        }
        .skill-items label {
            margin-left: 30px;
        }
    /* Customize website's scrollbar like Mac OS
Not supports in Firefox and IE */

/* total width */
.scrollbar::-webkit-scrollbar {
    background-color:transparent;
    width:16px
}

/* background of the scrollbar except button or resizer */
.scrollbar::-webkit-scrollbar-track {
    background-color:transparent
}
.scrollbar::-webkit-scrollbar-track:hover {
    background-color:#f4f4f4
}

/* scrollbar itself */
.scrollbar::-webkit-scrollbar-thumb {
    background-color:#babac0;
    border-radius:16px;
    border:4px solid #f4f4f4
}
.scrollbar::-webkit-scrollbar-thumb:hover {
    background-color:#a0a0a5;
    border:4px solid #f4f4f4
}

/* set button(top and bottom of the scrollbar) */
.scrollbar::-webkit-scrollbar-button {display:none}

/* div box */
@media(min-width: 768px){
  .scrollbar {
    position: fixed;
    top: 90px;
    height: 90vh;
    width: 23%;
    min-width: 150px;
    overflow-x: hidden;
    background: #f2f3f8;
    padding-bottom: 45px;
    overflow-y: scroll}
}
.m-subheader{
    display: none;
}
.candidates-container{
    margin-top: 10px;
}
        .user-skill {
            padding: 4px 8px;
            background: #ddd;
            margin: 0 0 5px 5px;
            border-radius: 16px;
            display: inline-block;
            font-size: 14px;
        }
        .bubble-link {
            color: #fff !important;
            text-decoration: none !important;
            padding: 5px 10px;
            background: #5867dd;
            border-radius: 16px;
            display: inline-block;
            font-size: 14px;
        }
.overflow{margin-right: 0px;}
        .candidate-id {
            position: absolute;
            top: 5px;
            left: 5px;
        }
    </style>
    <div class="m-content admin-content2222222">
        <div class="m-portlet m-portlet--mobile bg-admin">
            <div class="m-portlet__body">
                @if (Session::has('user_message'))
                    <div class="m-alert m-alert--outline alert alert-info alert-dismissible fade show"
                         role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                        {{Session::get('user_message')}}
                    </div>
                @endif
                <div class="row" id="candidates">
                    <div class="col-md-3">
                        @include('kandidate._filter')
                    </div>
                    <div class="col-md-9 col-lg-9">
                        <div class="loader_msg" style='display: block;'>
                            <img src="../assets/app/media/img/logos/loader.gif" width='132px' height='132px'
                                 style="height: 70px;width: 67px;margin-left: 40%;">
                        </div>
                        <button type="button" class="btn btn-info js-select-all" style="position: absolute;right: 592px; top: -95px;">Select</button>
                        <button type="button" class="btn btn-info" id="massEmail" @click="massEmail" style="position: absolute;right: 468px; top: -95px;">Send Email</button>
                        <div class="candidates-container">
                            <div class="row">
                                <div class="col-sm-6 col-md-4 col-lg-3 col-xl-3 candidate-profile" v-for="candidate in candidates" style="display: none">
                                    <div class="picture">
                                        @if(Auth::user()->isAdmin)
                                            <input type="checkbox" :value="candidate.raw.id" :id="`candidate-checkbox-${candidate.raw.id}`" v-model="checkedCandidates" class="candidate-id">
                                        @endif
                                        <a href="#" :data-id="`${candidate.raw.id}`" class="candidate-number js-candidate-number-link" :style="`background-color:${candidate.markBackground}`">@{{ candidate.raw.number }}</a>
                                        <img :src="candidate.image" alt="" style="width: 100%;">
                                        <table class="img-info" style="width: 100%;">
                                            <tr>
                                                <td><div>Gehalt: @{{ candidate.salaryExpectations.replace(/-/g, '&#8209;') }}</div></td>
                                                <td class="text-right"><div>Deutsch: @{{ candidate.linguisticProficiency }}</div></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="profile-details">
                                        <div class="firstName"><strong style="color: #000;">@{{ candidate.firstName }}</strong></div>
                                        <div class="Einsatzort"><strong style="color: #000;">Möglicher Einsatzort:</strong> @{{ candidate.possibleLocation }}</div>
                                        <div style="margin: 10px 0;">
                                            <span v-for="skill in candidate.techSkills" class="user-skill">@{{ skill }}</span>
                                        </div>
                                        <div class="profile-button">
                                            <a :href="candidate.viewUrl">Profil ansehen</a>
                                        </div>
                                        @if(Auth::user()->isAdmin)
                                        <table style="width: 100%">
                                            <tr>
                                                <td>
                                                    <a :href="candidate.cvUrl" class="bubble-link" target="_blank" v-if="candidate.cvUrl">CV herunterladen</a>
                                                </td>
                                                <td class="text-right">
                                                    <a :href="candidate.videoUrl" class="bubble-link" target="_blank" v-if="candidate.videoUrl">Video herunterladen</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <span v-if="candidate.isActive">Der Kandidat befindet sich noch auf der Suche</span>
                                                    <span v-if="!candidate.isActive">Der Kandidat hat bereits eine neue Stelle gefunden</span>
                                                </td>
                                            </tr>
                                        </table>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
    // if no Webkit browser
(function(){
  let isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
  let isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
  let scrollbarDiv = document.querySelector('.scrollbar');
})();
</script>
    <script type="text/javascript">var allCandidates = {!! json_encode($candidates) !!};</script>
@endsection