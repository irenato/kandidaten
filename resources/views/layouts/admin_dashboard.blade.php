@include('admin_dashboard_template.admin_header')
@include('admin_dashboard_template.admin_content')
@yield('content')
@component('components.cookies')
@endcomponent
@include('admin_dashboard_template.admin_footer')
@yield('css')
@yield('js')

