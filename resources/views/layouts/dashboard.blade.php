@include('template_parts.freelancer_header')
@include('template_parts.freelancer_content')
@yield('content')
@component('components.cookies')
@endcomponent
@include('template_parts.freelancer_footer')
@yield('css')
@yield('js')
