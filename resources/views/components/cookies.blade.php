<link rel="stylesheet" href="{{url('/css/cookies.css')}}">
<div class="cookies-message" id="cookie-box" style="display: none;">
    <div class="container">
        <h3>Diese Seite verwendet Cookies</h3>
        <p>Damit alle Bereiche dieser Website funktionieren und wir Dich von anderen Usern der Website unterscheiden können, verwenden wir Cookies. So können wir Dir beim Besuch unserer Website eine angenehme Erfahrung bieten und unsere Website laufend verbessern. Wenn Du damit einverstanden bist, klicke auf „Akzeptieren und weiter“. Wenn Du mehr über Cookies herausfinden möchtest oder diese Nachricht wiederholt siehst, lies <a href="/CookiePolicy-GERMANY.pdf" target="_blank"><nobr>unsere Cookie Policy</nobr></a>.</p>
        <a href="javascript:void(0)" class="btn btn-cookies" id="accept-cookies">AKZEPTIEREN UND WEITER</a>
    </div>
</div>
@section('inline-js')
    <script type="text/javascript">

        var popupcookie = {};

        popupcookie.init = function (){

            const cookieName = 'CookiesAccepted';
            /**
             * Accept cookies
             * Sets a cookie that cookies are accepted and closes the box
             */
            function acceptCookies(){
                const cookieBox = $('#cookie-box');
                $('#cookie-box').hide();
                Cookies.set(cookieName, true, { expires: 365 });
            }

            /**
             * Cookies accepted?
             * A boolean method to return cookies acceptance
             */
            function cookiesAccepted(){
                return Boolean(Cookies.get(cookieName));
            }


            /**
             * Binds cookie box controls to dom interactions
             */
            function bindCookieBox() {

                if(cookiesAccepted()) {
                    $('#cookie-box').hide();
                } else {
                    $('#cookie-box').show();
                }
            }

            bindCookieBox();
            $('#accept-cookies').bind('click', function(e) {
                e.preventDefault();
                acceptCookies();
            });

        }


        $(document).ready(function() {
            popupcookie.init();
        });
    </script>
@append