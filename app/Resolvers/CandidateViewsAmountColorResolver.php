<?php

declare(strict_types=1);

namespace App\Resolvers;


class CandidateViewsAmountColorResolver
{
    public const COLOURS = [
        'white',
        '#32CD32',
        'orange',
        '#FF4500'
    ];



    public function prepareBackgroundColour(int $viewsAmount): string
    {
        $step = 4;

        if ($step > $viewsAmount) {
            return self::COLOURS[$viewsAmount];
        }

        $viewsAmount = ($viewsAmount % $step);

        return self::COLOURS[$viewsAmount];
    }
}