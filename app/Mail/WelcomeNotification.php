<?php

namespace App\Mail;

use App\Kandidate;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WelcomeNotification extends Mailable
{
    use Queueable, SerializesModels;
    private $user;

    /**
     * FreelancerAvailabilityNotification constructor.
     * @param Kandidate $user
     */
    public function __construct(Kandidate $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject(config('app.name') . ': Welcome')
            ->bcc('honegg@argonstrategy.com')
            ->markdown('emails.welcome', [
                'user' => $this->user
            ]);
    }
}
