<?php

declare(strict_types=1);

namespace App\Exports;

use App\CompetenceSkill;
use App\Kandidate;
use Maatwebsite\Excel\Collections\SheetCollection;

class KandidateExport
{
    public function export(SheetCollection $collection): void
    {
        foreach ($collection->first() as $rowData) {
            $candidate = Kandidate::updateOrCreate(
                ['first_name' => $rowData->get('name')],
                [
                    'title' => '',
                    'first_name' => $rowData->get('name'),
                    'email' => $rowData->get('e_mail_kontakt'),
                    'hourly_rate' => $this->prepareHourlyRates(explode(',', $rowData->get('gehaltsvorstellung'))),
                    'role_definition' => $this->prepareRoles(explode(',', $rowData->get('rollendefinition'))),
                    'general_notes' => $rowData->get('allgemeine_informationen'),
                    'category_skills' => $this->prepareCategorySkills(explode(',', $rowData->get('faehigkeiten'))),
                    'availability_per_week' => $this->prepareAvailability(
                        explode(',', $rowData->get('sprachkenntnisse_deutsch'))
                    ),
                    'password' => bcrypt(time()),
                    'travelling' => '',
                    'traveling_state' => '',
                    'traveling_city' => '',
                    'availability_per_week_en' => ''
                ]
            );
            $candidate->save();
        }
    }

    private function prepareHourlyRates(array $inputData): string
    {
        $rates = array_flip(Kandidate::HOURLY_RATES_SHORT);
        $inputRates = [];

        foreach ($inputData as $rateTitle) {
            $inputRates[] = $rates[str_replace(' ', '', $rateTitle)] ?? null;
        }

        return implode(',', array_filter($inputRates));
    }

    private function prepareRoles(array $inputData): string
    {
        $roles = array_flip(Kandidate::ROLES);
        $inputItems = [];

        foreach ($inputData as $title) {
            $inputItems[] = $roles[preg_replace('/[^a-z]/i', '', $title)] ?? null;
        }

        return implode(',', array_filter($inputItems));
    }

    private function prepareCategorySkills(array $skillTitles): string
    {
        $skillsIds = [];

        foreach ($skillTitles as $skillTitle) {
            $existingSkill = CompetenceSkill::query()->whereRaw(
                "UPPER(`skill`) LIKE '%".strtoupper(trim($skillTitle))."%'"
            )->first();

            if (null !== $existingSkill) {
                $skillsIds[] = $existingSkill->id;
            }
        }

        return implode(',', array_filter($skillsIds));
    }

    private function prepareAvailability(array $inputData): string
    {
        $roles = array_flip(Kandidate::AVAILABILITY);
        $inputItems = [];

        foreach ($inputData as $title) {
            $inputItems[] = $roles[str_replace(' ', '', $title)] ?? null;
        }

        return implode(',', array_filter($inputItems));
    }
}