<?php

namespace App\Http\Controllers\Admin;

use App\Kandidate;
use App\Models\Comment;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CandidateCommentsController extends Controller
{

    public function store(Request $request, Kandidate $candidate)
    {
        $this->validate($request, [
            'comment' => 'required'
        ]);

        $timestamp = Carbon::now();

        $comment = $candidate->comments()->create([
            'comments' => $request->input('comment'),
            'timestamp' => $timestamp,
        ]);

        return response()->json([
            'status' => 'ok',
            'comment' => $comment,
        ]);
    }

    /**
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Kandidate $candidate)
    {
        $comments = $candidate->comments()
            ->orderBy('timestamp', 'desc')
            ->get();
        $comments = $comments->map(function(Comment $comment) {
            return [
                'id' => $comment->id,
                'timestamp_date' => $comment->timestamp->format('d-m-Y H:i:s'),
                'comment' => $comment->comments,
            ];
        });

        return response()->json($comments);
    }
    
    public function destroy(Comment $comment)
    {
        $comment->delete();
        
        return back();
    }

    public function remove(int $id): JsonResponse
    {
        $comment = Comment::findOrFail($id);

        return JsonResponse::create(['result' => $comment->delete()]);
    }
}