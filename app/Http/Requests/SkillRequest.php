<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SkillRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'skill' => 'required|unique:competences_skill,skill'
        ];
    }

    public function messages(): array
    {
        return [
            'skill.unique' => 'This skill already exists!'
        ];
    }
}
