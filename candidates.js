$(document).ready(function () {
    Vue.directive('select', {
        twoWay: true,
        bind: function (el, binding, vnode) {
            $(el).select2().on("select2:select", (e) => {
                // v-model looks for
                //  - an event named "change"
                //  - a value with property path "$event.target.value"
                el.dispatchEvent(new Event('change', {target: e.target}));
            });
            $(el).select2().on("select2:unselect", (e) => {
                // v-model looks for
                //  - an event named "change"
                //  - a value with property path "$event.target.value"
                el.dispatchEvent(new Event('change', {target: e.target}));
            });
        },
    });
    var candidates = new Vue({
        el: '#candidates',
        data: {
            salaryExpectations: [],
            roleDefinitions: [],
            techSkills: [],
            possibleLocations: [],
            linguisticProficiencyDe: [],
            linguisticProficiencyEn: [],
            candidates: allCandidates,
            checkedCandidates: [],
            is_confirmed: 'all',
            is_active: 'all',
        },
        computed: {
            filter: function () {
                return {
                    salaryExpectations: this.salaryExpectations,
                    roleDefinitions: this.roleDefinitions,
                    techSkills: this.techSkills,
                    possibleLocations: this.possibleLocations,
                    linguisticProficiencyDe: this.linguisticProficiencyDe,
                    linguisticProficiencyEn: this.linguisticProficiencyEn
                };
            }
        },
        watch: {
            salaryExpectations: function () {
                this.applyFilter()
            },
            roleDefinitions: function () {
                this.applyFilter()
            },
            techSkills: function () {
                this.applyFilter()
            },
            possibleLocations: function () {
                this.applyFilter()
            },
            linguisticProficiencyDe: function () {
                this.applyFilter()
            },
            linguisticProficiencyEn: function () {
                this.applyFilter()
            },
            is_confirmed: function () {
                this.applyFilter()
            },
            is_active: function () {
                this.applyFilter()
            }
        },
        methods: {
            massEmail: function () {
                let i = 0;
                this.checkedCandidates.map((candidateId) => {
                    let candidate = this.candidates.find(({raw: {id}}) => {
                        return id == candidateId;
                    });
                    setTimeout(function () {
                        let uri, subject, body, nameInfo;
                        nameInfo = candidate.raw.client_name || '';
                        subject = encodeURIComponent(nameInfo + ': Update for a new job offering in Germany');
                        body = encodeURIComponent(`Hello ${nameInfo}`);
                        uri = `mailto:${candidate.raw.email}?subject=${subject}`;
                        window.open(uri, '_blank');
                    }, i * 500);
                    i++;
                });
            },
            applyFilter: function () {
                let component = this;
                this.candidates = allCandidates.filter(({raw: {hourly_rate}}) => {
                    return component.applyFilterItem(component.salaryExpectations, hourly_rate.split(','));
                }).filter(({raw: {role_definition}}) => {
                    return component.applyFilterItem(component.roleDefinitions, role_definition.split(','))
                }).filter(({raw: {category_skills}}) => {
                    return component.applyFilterItem(component.techSkills, category_skills.split(','))
                }).filter(({raw: {travelling, traveling_state, traveling_city}}) => {
                    let possibleValues = [];
                    if (null !== travelling) {
                        possibleValues = travelling.split(',');
                    }
                    possibleValues.push(traveling_state, traveling_city);
                    return component.applyFilterItem(component.possibleLocations, possibleValues);
                }).filter(({raw: {availability_per_week}}) => {
                    return component.applyFilterItem(component.linguisticProficiencyDe, availability_per_week.split(','))
                }).filter(({raw: {availability_per_week_en}}) => {
                    return component.applyFilterItem(component.linguisticProficiencyEn, availability_per_week_en.split(','))
                }).filter(({isConfirmed}) => {
                    return component.is_confirmed === isConfirmed || component.is_confirmed === 'all';
                }).filter(({isActive}) => {
                    return component.is_active === isActive || component.is_active === 'all';
                });
            },

            applyFilterItem: function (selectedFilter, possibleValues) {
                if (!selectedFilter.length) {
                    return true;
                }

                for (let i in possibleValues) {
                    if (selectedFilter.includes(possibleValues[i])) {
                        return true;
                    }
                }

                return false;
            }
        },
        mounted() {
            tippy('[data-tippy-content]');
        }
    });


    $('#m_select2_9').select2({
        placeholder: "Select an option",
        maximumSelectionLength: 20
    });
    $('#m_select2_core').select2({
        placeholder: "Select an option",
        maximumSelectionLength: 20
    });

    $('#m_form_rate').select2({
        placeholder: "Select an option",
        maximumSelectionLength: 20
    });

    $(".js-candidate-number-link").on("click", function (e) {
        var _this = $(this);
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            type: 'put',
            url: 'kandidaten/mark/' + $(_this).data('id'),
            success: function (data) {
                $(_this).css('background-color', data.markBackground)
            },
        });
    })

    $('.js-select-all').on('click', function () {
        candidates.checkedCandidates = [];
        $('.candidate-id').each(function (i, el) {
            $(el).prop('checked', !$(el).prop('checked'));
            candidates.checkedCandidates.push($(el).val());
        });
    });
});
