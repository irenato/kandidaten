﻿$(document).ready(function () {
    load();
});

function load() {
    setTimeout(function () {
        $('.loaderContainer').addClass('load');
    }, 500);
    setTimeout(function () {
        $('.loaderContainer').removeClass('load');
        removeOverlay();
    }, 3200);
}

function removeOverlay() {
    $('.overlay').fadeOut('slow');
}

var lastId,
    topMenu = $(".add-scroll"),
    topMenuHeight = topMenu.outerHeight() + 75,
    // All list items
    menuItems = topMenu.find("a"),
    // Anchors corresponding to menu items
    scrollItems = menuItems.map(function () {
        var item = $($(this).attr("href"));
        if (item.length) { return item; }
    });

// Bind click handler to menu items
// so we can get a fancy scroll animation
menuItems.click(function (e) {
    var href = $(this).attr("href"),
        offsetTop = href === "#" ? 0 : $(href).offset().top - topMenuHeight + 1;
    offsetTop -= 100;
    $('html, body').stop().animate({
        scrollTop: offsetTop
    }, 600);
    e.preventDefault();
});

$('a').click(function (e) {
    var href = $(this).attr("href"),
        offsetTop = href === "#" ? 0 : $(href).offset().top;
    offsetTop -= 100;
    $('html, body').stop().animate({
        scrollTop: offsetTop
    }, 600);
    e.preventDefault();
});

// Bind to scroll
$(window).scroll(function () {
    // Get container scroll position
    var fromTop = $(this).scrollTop() + topMenuHeight;

    // Get id of current scroll item
    var cur = scrollItems.map(function () {
        if ($(this).offset().top < fromTop)
            return this;
    });
    // Get the id of the current element
    cur = cur[cur.length - 1];
    var id = cur && cur.length ? cur[0].id : "";

    if (lastId !== id) {
        lastId = id;
        // Set/remove active class
        menuItems
          .parent().removeClass("active")
          .end().filter("[href='#" + id + "']").parent().addClass("active");
    }
});

//Hamburger menu animation
$(".navbar-toggle").on("click", function () {
    $(".navbar-toggle").toggleClass("is-active");
});

$(function () {
    $('.nav a').on('click', function () {
        if ($('.navbar-toggle').css('display') != 'none') {
            $(".navbar-toggle").trigger("click");
        }
    });
});



var swiper = new Swiper('#referenzen .swiper-container', {
    autoplay: 5000,
    pagination: '.swiper-pagination',
    paginationClickable: true,
    autoplayDisableOnInteraction: false,
    paginationClickable: true
});


function initialize() {
    var styles = [
               {
                   featureType: "water",
                   elementType: "geometry",
                   stylers: [
                       {
                           color: "#e9e9e9"
                       },
                       {
                           lightness: 17
                       }
                   ]
               },
               {
                   featureType: "landscape",
                   elementType: "geometry",
                   stylers: [
                       {
                           color: "#f5f5f5"
                       },
                       {
                           lightness: 20
                       }
                   ]
               },
               {
                   featureType: "road.highway",
                   elementType: "geometry.fill",
                   stylers: [
                       {
                           color: "#ffffff"
                       },
                       {
                           lightness: 17
                       }
                   ]
               },
               {
                   featureType: "road.highway",
                   elementType: "geometry.stroke",
                   stylers: [
                       {
                           color: "#ffffff"
                       },
                       {
                           lightness: 29
                       },
                       {
                           weight: 0.2
                       }
                   ]
               },
               {
                   featureType: "road.arterial",
                   elementType: "geometry",
                   stylers: [
                       {
                           color: "#ffffff"
                       },
                       {
                           lightness: 18
                       }
                   ]
               },
               {
                   featureType: "road.local",
                   elementType: "geometry",
                   stylers: [
                       {
                           color: "#ffffff"
                       },
                       {
                           lightness: 16
                       }
                   ]
               },
               {
                   featureType: "poi",
                   elementType: "geometry",
                   stylers: [
                       {
                           color: "#f5f5f5"
                       },
                       {
                           lightness: 21
                       }
                   ]
               },
               {
                   featureType: "poi.park",
                   elementType: "geometry",
                   stylers: [
                       {
                           color: "#dedede"
                       },
                       {
                           lightness: 21
                       }
                   ]
               },
               {
                   elementType: "labels.text.stroke",
                   stylers: [
                       {
                           visibility: "on"
                       },
                       {
                           color: "#ffffff"
                       },
                       {
                           lightness: 16
                       }
                   ]
               },
               {
                   elementType: "labels.text.fill",
                   stylers: [
                       {
                           saturation: 36
                       },
                       {
                           color: "#333333"
                       },
                       {
                           lightness: 40
                       }
                   ]
               },
               {
                   elementType: "labels.icon",
                   stylers: [
                       {
                           visibility: "off"
                       }
                   ]
               },
               {
                   featureType: "transit",
                   elementType: "geometry",
                   stylers: [
                       {
                           color: "#f2f2f2"
                       },
                       {
                           lightness: 19
                       }
                   ]
               },
               {
                   featureType: "administrative",
                   elementType: "geometry.fill",
                   stylers: [
                       {
                           color: "#fefefe"
                       },
                       {
                           lightness: 20
                       }
                   ]
               },
               {
                   featureType: "administrative",
                   elementType: "geometry.stroke",
                   stylers: [
                       {
                           color: "#fefefe"
                       },
                       {
                           lightness: 17
                       },
                       {
                           weight: 1.2
                       }
                   ]
               }
    ];
    var myLatlng = new google.maps.LatLng(48.58834, 9.241651317);
    var mapOptions = {
        zoom: 14,
        scrollwheel: false,
        center: myLatlng
    }
    var map = new google.maps.Map(document.getElementById('map'), mapOptions);

    var image = 'images/map-marker.png';
    var myLatLng = new google.maps.LatLng(48.58834, 9.241651317);
    var beachMarker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        icon: image
    });
    map.setOptions({ styles: styles });

}
google.maps.event.addDomListener(window, 'load', initialize);