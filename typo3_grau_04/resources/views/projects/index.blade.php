<!DOCTYPE html>
<html lang="en">
<!-- begin::Head -->
<head>
    <meta charset="utf-8"/>
    <title>
        Argon | Festanstellung
    </title>
    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__rendered .select2-search__field {
            width: 125px !important;
        }
        .m-grid.m-grid--ver-desktop.m-grid--desktop > .m-grid__item.m-grid__item--fluid {
            flex: none !important;
            width: 80%;
        }
    </style>
@extends('layouts.admin_dashboard')
@section('content')
    <!-- END: Left Aside -->
        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <!-- BEGIN: Subheader -->
            <div class="m-subheader">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-page-title ">
                            <a href="{{ url('/admin/projects')}}">Projects</a>
                        </h3>
                    </div>
                </div>
            </div>
            @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert"
                     style="display: block; padding: 10px; margin:27px;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                    <p class="message">
                        {{session('status')}}
                    </p>
                </div>@endif
            <div class="m-portlet m-portlet--rounded view_block">
                <a  href="/admin/kandidaten" class="pull-right btn btn-info">Back</a>
                <a  href="{{route('projects.create')}}" class="pull-right btn btn-primary" style="margin-right: 20px;">Add project</a>
                <br>
                <br>
                <br>
                <table class="table table-bordered" style="width: 100%;">
                    <tr>
                        <th class="text-center" style="width: 100px;">#</th>
                        <th>Name</th>
                        <th style="width: 150px;"></th>
                    </tr>
                    @foreach($projects as $i => $project)
                        <tr style="background-color: #{{$i%2?'fafafa':'afafaf'}}">
                            <td class="text-center">{{$project->id}}</td>
                            <td>{{$project->name}}</td>
                            <td class="text-center">
                                <a href="{{route('projects.edit', $project)}}">Edit</a>
                                /
                                <a class="delete-project" onclick="$(this).next().submit(); return false;" href="#">Delete</a>
                                <form action="{{route('projects.destroy', $project)}}" method="post">
                                    {{method_field('DELETE')}}
                                    {{csrf_field()}}
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <!-- end:: Body -->

            <!-- begin::Scroll Top -->
            <div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500"
                 data-scroll-speed="300">
                <i class="la la-arrow-up"></i>
            </div>
        </div>
        <!-- end::Scroll Top -->
@endsection
