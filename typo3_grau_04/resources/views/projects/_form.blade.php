{{ csrf_field() }}
<style>
    .has-error input {
        border-color: red;
    }
    .has-error .help-block {
        color: red;
    }
</style>
<div class="m-portlet__body">
    <div class="form-group m-form__group row{{ $errors->has('name') ? ' has-error' : '' }}">
        <label class="col-lg-4 col-form-label text-left">
            <font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Name</font></font>
        </label>
        <div class="col-lg-8 ref_input">
            <input type="text" name="name" id="name" class="form-control m-input"
                   placeholder="Enter Project Name" value="{{old('name', $project->name)}}" required autofocus>
            @if ($errors->has('name'))
                <span class="help-block">
                   <strong>{{ $errors->first('number') }}</strong>
               </span>
            @endif
        </div>
    </div>

    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
        <div class="m-form__actions m-form__actions--solid">
            <div class="row">
                <div class="col-lg-4"></div>
                <div class="col-lg-8">
                    <a class="btn btn-secondary pull-right" href="{{ route('projects.index') }}">Cancel</a>
                    <button type="submit" class="btn btn-primary pull-right" style="margin-right: 20px;">
                        @if($project->id)
                            Update
                        @else
                            Add
                        @endif
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>